<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class MultiplicationExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('multiplication', [$this, 'multiplication']),
        ];
    }

    public function multiplication($argOne, $argTwo)
    {
        $result = $argOne * $argTwo;

        return $result;
    }
}