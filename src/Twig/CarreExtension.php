<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CarreExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('carre', [$this, 'carre']),
        ];
    }

    public function carre($argOne)
    {
        $result = $argOne * $argOne;

        return $result;
    }
}