<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {

        $pannierECommerce = ['Macbook', 'Clavier silencieux', 'Iphone'];
        // Ici j'envoie un mail
        $message = (new \Swift_Message('Ouf je suis pas si nul ... '))
            ->setFrom('testultrasayan@gmail.com')
            ->setTo('aureliendelorme1@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/hello_world.html.twig'
                ),
                'text/html'
            );


        $mailer->send($message);

        return $this->render('default/index.html.twig', [
            'name'=> 'Toto',
            'pannier'=> $pannierECommerce
        ]);
    }

    /**
     * @Route("/request", name="print_request")
     */
    public function printRequest(Request $request){
        return $this->render('default/index.html.twig', [
            'controller_name' => 'Toto',
        ]);
    }

    /**
     * @Route("/json-response", name="display_json")
     */
    public function returnJson(){
        $array = ['success'=> true];
        return new JsonResponse($array);
    }

    /**
     * @Route("/binary", name="binary_response")
     */
    public function binaryResponse(){
        $file = 'test.txt';
        $response = new BinaryFileResponse($file);
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'fichier_telecharger.txt'
        );
        return $response;
    }

    /**
     * @Route("/stream", name="stream_reponse")
     */
    public function streamResponse(){
        $response = new StreamedResponse();
        $response->setCallback(function () {
            var_dump('Hello World');
            flush();
            sleep(2);
            var_dump('Hello World');
            flush();
        });

        $response->send();

        return new Response('done');
    }

    /**
     * @Route("/redirect", name="redirect_response")
     */
    public function redirectResponse(){
      return new RedirectResponse('http://www.google.fr');
    }


    /**
     * @Route("/add-session", name="set_session.html.twig")
     */
    public function setSession(Request $request){
        $session = $request->getSession();
        $session->set('identity', ['lastname'=>'Aurélien', 'firstname'=>'Delorme']);

        return $this->render('session/set_session.html.twig');
    }

    /**
     * @Route("/display-session", name="display_session")
     */
    public function displaySession(Request $request){
        $session = $request->getSession();
        return $this->render('session/display_session.html.twig', ['identity'=> $session->get('identity')]);
    }

    /**
     * @Route("/add-cookie", name="set_cookie.html.twig")
     */
    public function setCookie(){
        $response = new Response('ajout d\'un cookie');
        $response->headers->setCookie(new Cookie('firstname', 'aurelien'));
        $response->headers->clearCookie('connect');
        return $response;
    }

    /**
     * @Route("/display-cookie", name="display_cookie")
     */
    public function displayCookie(){

        return $this->render('cookie/display.html.twig');
    }


}
